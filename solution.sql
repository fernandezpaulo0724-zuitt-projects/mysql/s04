-- Find all artists that has D in its name
SELECT * FROM songs WHERE song_name LIKE "%a%";

-- Find all songs with length less than 230
SELECT * FROM songs WHERE length < 230;

-- join the albums and songs tables (only show the album name, song name, and song length)
SELECT albums.album_title, songs.song_name, songs.length
	FROM albums JOIN songs ON albums.id = songs.album_id;

-- join the artists and albums tables (find all albums that has letter a in its name)
SELECT * FROM albums
    JOIN artists ON artists.id = albums.artist_id WHERE albums.album_title LIKE "%a%";

-- sort the albums in z-a order (show only the first 4 records)
SELECT * FROM songs ORDER BY song_name DESC LIMIT 4;

-- join the albums and songs tables (sort albums from z-a and sort songs from a-z)
SELECT * FROM albums
    JOIN songs ON albums.id = songs.album_id
    ORDER BY albums.album_title DESC, songs.song_name ASC;